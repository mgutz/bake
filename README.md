# Bake

Simple build/command utility for bash.

Tired of being a PHONY.


## Installation

    npm install bake-bash

Or,

    git clone https://github.com/mgutz/bake
    sudo ln -sf `pwd`/bake/bake.sh /usr/local/bin/bake

# Usage

Display tasks

    bake

Run a task

    bake <task>

## Rules

* `bake` searches the current and parent directories for a `Bakefile` to run.
* A task is marked by using `@task` in the function commment.

## Functions

Prints a red error message.

    bake_error <action> <description>

    example: bake_error "compiling" "src/lib/test.coffee"

Prints a plain message.

    bake_log <action> <description>

    example: bake_log "bake" "Processing bakefile..."

Prints a green ok message.

    bake_ok <action> <descsription>

    example: bake_ok "compiling" "compiled src/lib/test.js"

Prints a cyan info message.

    bake_info <action> <description>

    example: bake_info "bake" "built project in 700ms"

Invokes a task only once.

    invoke <function_name>

    example: invoke "clean"

Determines if target is older than reference, returning 1 if outdated.

    outdated <target> <reference>

    examples:

    outdated build src || return 1          # skip rest of task
    outdated build src && invoke "compile"  # compile if outdated


Run a function when command is not found. Add a function like

    on_invalid_command () {
        [[ -f test/$1.js ]] && nodeunit test/$1.js && return 0
        return 1
    }



## Example

Example Bakefile

    private() {
        echo in private
    }


    #. Cleans the project.
    clean() {
        echo cleaning ...
        _private
    }


    #. Builds the project.
    build() {
        # invokes a function once, otherwise call function directly
        invoke "clean"
        echo building ...
    }


    #. Compiles coffee scripts
    coffeescripts() {
        outdated build src || return 0
        coffee -c -o build src
        bake_ok "coffee" "compiled"
    }
