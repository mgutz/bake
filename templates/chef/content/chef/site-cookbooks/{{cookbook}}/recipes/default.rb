# Template example using ../templates/default/foo.erb
# Also uses SUSER which is set when ../libraries/user.rb is loaded (automatically)
template "#{SHOME}/tmp/foo" do
  source "foo.erb"
  owner SUSER
end

